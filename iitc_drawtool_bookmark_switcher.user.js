// ==UserScript==
// @id             iitc-drawtool-bookmark-switcher@quantenstorm
// @name           IITC plugin: Drawtool Bookmark Switcher
// @category       Layer
// @version        1.0.3.20181129
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @description    [quantenstorm-2018-11-29] Switch easy between different draws and bookmarks.
// @match          /^https?:\/\/intel.ingress\.com\/?((intel|mission|\?).*)?$/
// @include        /^https?:\/\/intel.ingress\.com\/?((intel|mission|\?).*)?$/
// @grant          none

// ==/UserScript==


function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function') window.plugin = function() {};


    // PLUGIN START ////////////////////////////////////////////////////////////

    // use own namespace for plugin
    window.plugin.drawbooksw = function() {};
    var drawbooksw = window.plugin.drawbooksw;

    drawbooksw.shortId = function(longId) {
        // split in letters and numbers
        var parts = longId.match(/([A-Za-z]+)([0-9]+)/);
        // create short IDs (D1, D2, B1, ...) from long IDs (drawstore1, ...)
        return parts[1][0].toUpperCase() + parts[2];
    }

    drawbooksw.changeStorage = function(storeId){
        var number = drawbooksw.shortId(storeId.parentNode.id);

        if (number[0] === 'D') {
            // save current map position in the 'old' storage
            var index = drawbooksw.settings.dLayer.findIndex(x => x.number == drawbooksw.settings.dLatest);
            drawbooksw.settings.dLayer[index].map = map.getCenter();
            drawbooksw.settings.dLayer[index].zoom = parseInt(map.getZoom());
            localStorage['draw-book-switcher'] = JSON.stringify(drawbooksw.settings);

            // load new data
            drawbooksw.settings.dLatest = number;
            drawbooksw.loadDraw();

            // zoom to last map position of the 'new' storage
            var store = JSON.parse(localStorage['draw-book-switcher']);
            index = store.dLayer.findIndex(x => x.number == number);
            map.setView(store.dLayer[index].map, store.dLayer[index].zoom);

            // change buttonlabel
            buttondraw.removeChild(buttondraw.childNodes[0]);
            buttondraw.appendChild(document.createTextNode(number));
            console.log('Switched to draw ' + number);

        } else if (number[0] === 'B') {
            // load new data
            drawbooksw.settings.bLatest = number;
            drawbooksw.loadBookmarks();
            console.log('Switched to bookmarks ' + number);

            // change buttonlabel
            buttonbook.removeChild(buttonbook.childNodes[0]);
            buttonbook.appendChild(document.createTextNode(number));
        }
    }

    drawbooksw.newStorage = function(layerType) {
        layerType = layerType.id[0].toUpperCase();
        var numbers = [];
        var layer = [];

        if (layerType === 'D') {
            layer = drawbooksw.settings.dLayer;
        } else if (layerType === 'B') {
            layer = drawbooksw.settings.bLayer;
        }

        // get the numbers/ids of storages
        for (var i = 0; i < layer.length; i++) {
            numbers.push(parseInt(layer[i].number.substring(1)));
        }
        numbers.sort();

        for (var i = 1; i <= numbers.length + 1; i++) {
            // find out whitch number in the row is missing and take it
            if (numbers.indexOf(i) == -1) {
                if (layerType === 'D') {
                    var number = 'D' + i;
                    var name = prompt("Please enter new draw layer name", 'Draw ' + i);

                    if (name !== null) {
                        // set and save settings and new storage
                        drawbooksw.settings.dLayer.push({number:number, name:name, map:map.getCenter(), zoom:parseInt(map.getZoom())});
                        localStorage['plugin-draw-tools-layer-' + number] = '';
                    } else {
                        alert("error! you have not enter a name");
                    }

                } else if (layerType === 'B') {
                    var number = 'B' + i;
                    var name = prompt("Please enter new bookmark layer name", 'Bookmark ' + i);

                    if (name !== null) {
                        // set and save settings and new storage
                        drawbooksw.settings.bLayer.push({number:number, name:name});
                        localStorage['plugin-bookmarks-' + number] = '{"maps":{"idOthers":{"label":"Others","state":1,"bkmrk":{}}},"portals":{"idOthers":{"label":"Others","state":1,"bkmrk":{}}}}';
                    } else {
                        alert("error! you have not enter a name");
                    }
                }

                localStorage['draw-book-switcher'] = JSON.stringify(drawbooksw.settings);
                drawbooksw.chooser();
                break;
            }
        }
    }

    drawbooksw.deleteStorage = function(storeId) {
        var number = drawbooksw.shortId(storeId.parentNode.id);

        if (number !== drawbooksw.settings.dLatest && number !== drawbooksw.settings.bLatest) {
            if (number[0] === 'D') {
                // delete layer from settings
                index = drawbooksw.settings.dLayer.findIndex(x => x.number == number);
                drawbooksw.settings.dLayer.splice(index, 1);

                // delete localstorage of the draw
                localStorage.removeItem('plugin-draw-tools-layer-' + number);
            } else if (number[0] === 'B') {
                // delete layer from settings
                index = drawbooksw.settings.bLayer.findIndex(x => x.number == number);
                drawbooksw.settings.bLayer.splice(index, 1);

                // delete localstorage of the draw
                localStorage.removeItem('plugin-bookmarks-' + number);
            }

            localStorage['draw-book-switcher'] = JSON.stringify(drawbooksw.settings);
            drawbooksw.chooser();
        } else {
            alert('You cannot delete the storage you are currently working on!\nFirst select an other one and try again.');
        }
    }

    drawbooksw.renameStorage = function(storeId) {
        var number = drawbooksw.shortId(storeId.parentNode.id);
        var oldName = storeId.previousSibling.innerText

        var newName = prompt("Please enter new layer name", oldName);
        if (newName !== null) {
            if (number[0] === 'D') {
                index = drawbooksw.settings.dLayer.findIndex(x => x.number == number);
                drawbooksw.settings.dLayer[index].name = newName;
            } else if (number[0] === 'B') {
                index = drawbooksw.settings.bLayer.findIndex(x => x.number == number);
                drawbooksw.settings.bLayer[index].name = newName;
            }

            localStorage['draw-book-switcher'] = JSON.stringify(drawbooksw.settings);
            drawbooksw.chooser();
        } else {
            alert("error! you have not enter a name");
        }
    }


    // DRAWTOOLS ///////////////////////////////////////////////////////////////

    drawbooksw.updateDraw = function() {
        localStorage['plugin-draw-tools-layer-' + drawbooksw.settings.dLatest] = localStorage['plugin-draw-tools-layer'];
    }

    drawbooksw.loadDraw = function() {
        window.plugin.drawTools.drawnItems.clearLayers();
        localStorage['plugin-draw-tools-layer'] = localStorage['plugin-draw-tools-layer-' + drawbooksw.settings.dLatest];
        window.plugin.drawTools.load();
    }


    // BOOKMARKS ///////////////////////////////////////////////////////////////

    drawbooksw.updateBookmark = function() {
        localStorage['plugin-bookmarks-' + drawbooksw.settings.bLatest] = localStorage[window.plugin.bookmarks.KEY_STORAGE];
    }

    drawbooksw.loadBookmarks = function() {
        localStorage[window.plugin.bookmarks.KEY_STORAGE] = localStorage['plugin-bookmarks-' + drawbooksw.settings.bLatest];
        window.plugin.bookmarks.refreshBkmrks();
        window.runHooks('pluginBkmrksEdit', {"target": "all", "action": "import"});
    }

    ////////////////////////////////////////////////////////////////////////////

    // Dialog to choose draws and bookmarks
    drawbooksw.chooser = function() {
        $('head').append('<style>'
        + '.drawBookSwitcher { display:flex; margin:0 auto; }'
        + '.drawBookSwitcher > span { padding: 3px 0; margin: 5px 0; }'
        + '.drawBookSwitcher > a { color:#ffce00; border:1px solid #ffce00; padding:3px 3px; margin:5px auto; width:auto; text-align:left; background:rgba(8,48,78,.9); }'
        + '.drawBookSwitcher > a:nth-of-type(1) { width:70%; }'
        + '.newStore { width:auto !important;}'
        + '</style>');

        var html = '<div><b>Draws</b></div>'
        // all drawtools layer
        for (var i = 0; i < drawbooksw.settings.dLayer.length; i++) {
            var id = 'drawStore' + drawbooksw.settings.dLayer[i].number.substring(1);
            var number = drawbooksw.settings.dLayer[i].number;
            var name = drawbooksw.settings.dLayer[i].name;

            html += '<div id="' + id + '" class="drawBookSwitcher">'
                + '<span>' + number + ':</span>'
                + '<a onclick="window.plugin.drawbooksw.changeStorage(this);">' + name + '</a>'
                + '<a onclick="window.plugin.drawbooksw.renameStorage(this)">&#x270E;</a>'
                + '<a onclick="window.plugin.drawbooksw.deleteStorage(this)">&#x1F5D1;</a></div>';
        }

        html += '<div class="drawBookSwitcher"><a id="dNewStore" class="newStore" onclick="window.plugin.drawbooksw.newStorage(this)">++ new draw storage ++</a></div>'
            + '<div style="margin-top:10px"><b>Bookmarks</b></div>'

        // all bookmarks layer
        for (var i = 0; i < drawbooksw.settings.bLayer.length; i++) {
            var id = 'bookStore' + drawbooksw.settings.bLayer[i].number.substring(1);
            var number = drawbooksw.settings.bLayer[i].number;
            var name = drawbooksw.settings.bLayer[i].name;

            html += '<div id="' + id + '" class="drawBookSwitcher">'
                + '<span>' + number + ':</span>'
                + '<a onclick="window.plugin.drawbooksw.changeStorage(this);">' + name + '</a>'
                + '<a onclick="window.plugin.drawbooksw.renameStorage(this)">&#x270E;</a>'
                + '<a onclick="window.plugin.drawbooksw.deleteStorage(this)">&#x1F5D1;</a></div>';
        }

        html += '<div class="drawBookSwitcher"><a id="bNewStore" class="newStore" onclick="window.plugin.drawbooksw.newStorage(this)">++ new bookmark storage ++</a></div>';

        dialog({
            html: html,
            id: 'plugin-chooser',
            dialogClass: 'ui-dialog-chooser',
            title: 'Choose draws and bookmarks'
        });
    }


    // SETUP ///////////////////////////////////////////////////////////////////

    var setup = function() {
        if (window.plugin.drawTools === undefined) {
            alert("'Drawtool-Switcher' requires plugin 'draw-tools'");
            return;
        }
        if (window.plugin.bookmarks === undefined) {
            alert("'Bookmark-Switcher' requires plugin 'bookmarks'");
            return;
        }

        // load settings
        if (localStorage['draw-book-switcher']) {
            drawbooksw.settings = JSON.parse(localStorage['draw-book-switcher']);
        } else {
            drawbooksw.settings = {};
            drawbooksw.settings.dLayer = [{number:'D1', name:'Draw 1', map:map.getCenter(), zoom:parseInt(map.getZoom())}];
            drawbooksw.settings.dLatest = 'D1';
            drawbooksw.settings.bLayer = [{number:'B1', name:'Bookmark 1'}];
            drawbooksw.settings.bLatest = 'B1';

            localStorage['draw-book-switcher'] = JSON.stringify(drawbooksw.settings);
        }

        // create Buttons
        parent = $(".leaflet-top.leaflet-left", window.map.getContainer());

        buttondraw = document.createElement("a");
        buttondraw.className = "leaflet-bar-part";
        buttondraw.addEventListener("click", drawbooksw.chooser, false);
        buttondraw.title = 'choose draws and bookmarks';
        var textdraw = document.createTextNode(drawbooksw.settings.dLatest);
        buttondraw.appendChild(textdraw);

        buttonbook = document.createElement("a");
        buttonbook.className = "leaflet-bar-part";
        buttonbook.addEventListener("click", drawbooksw.chooser, false);
        buttonbook.title = 'choose draws and bookmarks';
        var textbook = document.createTextNode(drawbooksw.settings.bLatest);
        buttonbook.appendChild(textbook);

        container = document.createElement("div");
        container.className = "leaflet-bookdrawsw leaflet-bar leaflet-control";
        container.appendChild(buttondraw);
        container.appendChild(buttonbook);
        parent.append(container);

        // add hooks
        window.addHook('pluginDrawTools', e => {
            // drawtools fires the import hook before it has save the data in the localStorage.
            // because of this we wait a bit until we believe drawtool has saved it.
            if (e.event == 'import') {
                setTimeout(x => {
                    drawbooksw.updateDraw();
                }, 2000);
            } else {
                drawbooksw.updateDraw();
            }
        });
        window.addHook('pluginBkmrksEdit', e => drawbooksw.updateBookmark());
    }

    // PLUGIN END //////////////////////////////////////////////////////////////


    setup.info = plugin_info; //add the script info data to the function as a property
    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);
